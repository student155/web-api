const express = require("express"); // import express
const data = require("./data.json"); // import weather data
const app = express(); // initialize express
const cors = require("cors"); // import cors package

app.use(cors());

// Listen on port 3000
app.listen(3000, function () {
  console.log("listening on port 3000...");
});

app.get("/", (request, response) => {
  response.send("Hello World");
});
