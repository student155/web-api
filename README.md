# Web-API (Fake Weather App)

In deze oefening gaan we beginnen met backend-development door een eenvoudige Web-API te ontwikkelen die (fictieve) weer-data beschikbaar stelt. Dit repository bevat een opzet van een (RESTful) Web-API gemaakt in de programmeertaal [Javascript](https://www.w3schools.com/js/DEFAULT.asp) met [Node.js](https://nodejs.org/en/) en het [Express](https://expressjs.com/) framework. Via een Web-API kun je data ophalen en opslaan via een (HTTP-)netwerkverbinding in een client-server of multi-tier applicatie-architectuur (zie de Leeractiviteit 'Software Architectuur en Infrastructuur' op Canvas). Omdat de gegevens op een centrale plek (web-server) staan, hebben alle apps toegang tot dezelfde data.

**Wat gaan we leren?**
- Opzetten van een simpele [RESTful](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design) Web-API
- Werken met Javascript
- Werken met JSON
## Beginnen

Om te beginnen heb je Node.js op je computer nodig. Node.js zorgt ervoor dat je computer Javascript-code buiten de browser kan uitvoeren. Dit kun je checken door `node --version` in te typen in je terminal. Verschijnt er géén versienummer, dan kun je Node.js [hier](https://nodejs.org/en/) downloaden.

Vervolgens clone je dit Git-repository op je computer om te beginnen:
`git clone git@gitlab.com:student155/web-api.git`

Navigeer naar het respository:
`cd web-api`

Installeer de dependencies (Express) via de Node Package Manager (npm):
`npm install`

Nu ben je klaar om de Web-API op te starten door de Javascript code in het bestand `index.js` uit te voeren met Node:
`node index.js`

Als het goed is zie `listening on port 3000...`. Dit betekent dat de webserver lokaal op je computer is opgestart en luistert naar binnenkomend verkeer op de netwerkpoort 3000.

Wanneer je in nu in je browser naar de url `http://localhost:3000/` navigeert zie je als het goed is de boodschap 'Hello World'. Dit is de tekst die de webserver teruggeeft op het `/` (root) endpoint (zie regel 11 in `index.js`).

## Opdracht

1. Zorg dat de Web-API alle (fictieve) weergegevens in het [JSON](https://www.json.org/json-en.html) data in het bestand `data.json` teruggeeft op het endpoint `/weather/all` (dus via de URL `http://localhost:3000/weather`, zie [Express: Routing](https://expressjs.com/en/guide/routing.html)).

2. Voeg handmatig (fictieve) weergegevens van één of meerdere steden toe aan het JSON-bestand (bijv. van Rotterdam of Groningen, zie [JSON](https://www.json.org/json-en.html)).

3. Voeg een endpoint toe waarmee het weer in één bepaalde locatie kunnen opvragen, via het endpoint `/weather?city=<stad>`, dus bijvoorbeeld `/weather?city=amersfoort`.

## Verdere opties

* Voeg aanvullende (fictieve) data toe aan je API-endpoint, bijvoorbeeld neerslag, luchtvochtigheid en eventuele gladheid (voor voorbeeld, zie branch [additional-info](https://gitlab.com/student155/web-api/-/tree/additional-info)).

* Voeg een programmatische conversie toe van Celsius naar Fahrenheit (voor voorbeeld, zie branch [convert-celsius-to-fahrenheit](https://gitlab.com/student155/web-api/-/tree/convert-celsius-to-fahrenheit)).

* Voeg een POST-endpoint (`/weather`) toe waarmee we weer-data kunnen toevoegen aan het bestand door JSON mee te sturen met een POST-request (voor voorbeeld, zie branch [add-post-endpoint](https://gitlab.com/student155/web-api/-/tree/add-post-endpoint)).

* Sla de data op in een [SQLite](https://www.sqlite.org/index.html) database, i.p.v. een tekstbestand (voor voorbeeld, zie branch [sqlite](https://gitlab.com/student155/web-api/-/tree/sqlite)).

* Zorg voor een correcte foutafhandeling door de juiste HTTP status-codes mee te sturen wanneer er iets fout gaat (wanneer er bijvoorbeeld een ongeldig request binnenkomt, zie [Express: Error Handling](https://expressjs.com/en/guide/error-handling.html)) (voor voorbeeld, zie branch [add-error-handling](https://gitlab.com/student155/web-api/-/tree/add-error-handling)).

* Maak een (tweede) front-end voor de bestaande backend, b.v. een desktop-app, een mobiele app of een andere web-app.

* Voeg één of meerdere unit tests toe m.b.v. [Jest](https://jestjs.io/).

* (Her)implementeer de API in [Typescript](https://www.typescriptlang.org/).

## Meer informatie
- [Express: Getting Started](https://expressjs.com/en/starter/installing.html)
- [Express: Routing](https://expressjs.com/en/guide/routing.html)
- [Express: Error Handling](https://expressjs.com/en/guide/error-handling.html)
- [Javascript Tutorial](https://www.w3schools.com/js/DEFAULT.asp)
- [JSON](https://www.json.org/json-en.html)
- [RESTful web API design](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design)